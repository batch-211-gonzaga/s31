const http = require('http'); // #5

const PORT = 3000 // #6

const server = http.createServer((request, response) => { // #7
  if (request.url == '/login') { // #9
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to the login page.')
  } else { // #11
    response.writeHead(404, { 'Content-Type': 'text/plain' });
    response.end('I\'m sorry the page you are looking for canot be found.');
  }
});

server.listen(PORT); // #7b
console.log(`Server running on port ${PORT} [http://localhost:${PORT}]`); // #8
