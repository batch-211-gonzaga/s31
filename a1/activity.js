/* 3.

a. What directive is used by Node.js to load the modules it needs?
require()

b. What Node.js module contains a method for server creation?
http

c. What is the method of the http object responsible for creating a server using Node.js?
createServer()

d. What method of the response object allows us to set status codes and content types?
writeHead()

e. Where will consle.log() output contents when run in Node.js?
terminal

f. What property of the request object contains the address' endpoint?
url

*/
